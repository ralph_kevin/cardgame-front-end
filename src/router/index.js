import Home from "@/views/home";
import PageNotFound from "@/views/page-not-found";
import SignIn from "@/views/sign-in";
import SignUp from "@/views/sign-up";
import SignUpVerification from "@/views/sign-up-verification";
import PlayCardAssessment from "@/views/play/card-assessment";
import PlayCardSummary from "@/views/play/card-summary";
import PlayCardResearch from "@/views/play/card-confirmation";
import PlayCardResult from "@/views/play/card-result";
import Welcome from "@/views/welcome";
import Dashboard from "@/views/dashboard";
import Result from "@/views/result";
import Account from "@/views/account";
import AccountDetails from "@/views/account-details";
import ChangePassword from "@/views/change-password";
import UpdatePlan from "@/views/update-plan";
import Packages from "@/views/packages";
import Payment from "@/views/payment-success";
import Terms from "@/views/terms-and-condition";
import Privacy from "@/views/privacy-policy";
import Cookies from "@/views/cookies";
import Checkout from "@/views/pay/checkout";
export const routes = [
  {
    path: "/",
    component: Home
  },
  {
    path: "*",
    component: PageNotFound,
    meta: { hideNavigation: true, showFooter: false }
  },

  //USERS
  { path: "/login", component: SignIn, meta: { hideNavigation: true } },
  { path: "/register", component: SignUp, meta: { hideNavigation: true } },
  {
    path: "/verification",
    component: SignUpVerification,
    meta: { hideNavigation: true }
  },
  {
    path: "/welcome",
    component: Welcome,
    meta: {
      hideNavigation: false,
      requiredLogged: true,
      showFooter: true,
      requiredSubscription: true
    }
  },
  {
    path: "/dashboard",
    component: Dashboard,
    meta: {
      hideNavigation: false,
      requiredLogged: true,
      showFooter: true,
      requiredSubscription: true
    }
  },
  {
    path: "/result",
    component: Result,
    meta: {
      hideNavigation: false,
      requiredLogged: true,
      showFooter: true,
      requiredSubscription: true
    }
  },
  {
    path: "/account",
    component: Account,
    meta: {
      hideNavigation: false,
      requiredLogged: true,
      showFooter: true,
      requiredSubscription: true
    }
  },
  {
    path: "/account-details",
    component: AccountDetails,
    meta: {
      hideNavigation: false,
      requiredLogged: true,
      showFooter: true,
      requiredSubscription: true
    }
  },
  {
    path: "/change-password",
    component: ChangePassword,
    meta: {
      hideNavigation: false,
      requiredLogged: true,
      showFooter: true,
      requiredSubscription: true
    }
  },
  {
    path: "/update-plan",
    component: UpdatePlan,
    meta: {
      hideNavigation: false,
      requiredLogged: true,
      showFooter: true,
      requiredSubscription: true
    }
  },
  //PLAY
  {
    path: "/play/card-assessment",
    component: PlayCardAssessment,
    meta: { hideNavigation: true, requiredLogged: true, requiredSubscription: true }
  },
  {
    path: "/play/card-summary",
    component: PlayCardSummary,
    meta: { hideNavigation: true, requiredLogged: true }
  },
  {
    path: "/play/card-research",
    component: PlayCardResearch,
    meta: { hideNavigation: true, requiredLogged: true }
  },
  {
    path: "/play/card-result/:resultId",
    component: PlayCardResult,
    meta: { hideNavigation: true, requiredLogged: true }
  },
  //Packages
  {
    path: "/packages",
    component: Packages,
    meta: { hideNavigation: false, requiredLogged: true, showFooter: true }
  },
  {
    path: "/payment-success",
    component: Payment,
    meta: { hideNavigation: true, requiredLogged: true, showFooter: false }
  },
  // terms
  {
    path: "/terms-and-condition",
    component: Terms,
    meta: { hideNavigation: false, showFooter: false }
  },
  {
    path: "/privacy-policy",
    component: Privacy,
    meta: { hideNavigation: false, showFooter: false }
  },
  {
    path: "/cookies",
    component: Cookies,
    meta: { hideNavigation: false, showFooter: false }
  },
  // Pay
  {
    path: "/pay/checkout",
    component: Checkout,
    meta: { hideNavigation: false, requiredLogged: true, showFooter: false }
  }
];
