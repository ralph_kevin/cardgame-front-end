<template>
    <div class="card-parent-result">
         <Loader :loading="loading"/>
        <h2 class="title">{{ $t('Assessment Result') }}</h2>

        <div class="card-list">
            <div
                class="card-big"
                v-for="(card, index) in result.slice(0,3)"
                :key="'card-big' + index"
            >
                <div class="top">{{ $t('Top') }} {{ index + 1 }}</div>
                <div class="content" @click="handleOpenCard(card)">
                    <img :src="require(`@/assets/results/${card.number}_${card.names}-front.png`)" />
                    <div class="percent">
                        <vue-circle
                            :progress="getIndividualPercent(card) "
                            :size="60"
                            :reverse="false"
                            line-cap="round"
                            :fill="'#00AEEF'"
                            empty-fill="#fff"
                            :animation-start-value="0.0"
                            :start-angle="0"
                            insert-mode="append"
                            :thickness="3"
                            :show-percent="false"
                        >   <div class="percent-label">
                                <span class="percent-text">{{ getIndividualPercent(card) }}</span>
                                <span class="percent-perc">%</span>
                            </div>
                        </vue-circle>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-list">
            <div
                class="card-small"
                v-for="(card, index) in result.slice(3,7)"
                :key="'card-small' + index"
            >
                <div class="top">Top {{ index + 4 }}</div>
                <div class="content" @click="handleOpenCard(card)">
                    <img :src="require(`@/assets/results/${card.number}_${card.names}-front.png`)" />
                    <div class="percent">
                        <vue-circle
                            :progress="getIndividualPercent(card) "
                            :size="60"
                            :reverse="false"
                            line-cap="round"
                            :fill="'#00AEEF'"
                            empty-fill="#fff"
                            :animation-start-value="0.0"
                            :start-angle="0"
                            insert-mode="append"
                            :thickness="3"
                            :show-percent="false"
                        >   <div class="percent-label">
                                <span class="percent-text">{{ getIndividualPercent(card) }}</span>
                                <span class="percent-perc">%</span>
                            </div>
                        </vue-circle>
                    </div>
                </div>
            </div>
        </div>

        <div class="button">
            <button type="button" class="btn btn-outline-primary btn-lg" @click="returnHome()">{{ $t('Return Home') }}</button>
        </div>

        <div class="confeti confeti1">
            <img :src="require('@/assets/confetti_left.png')"/>
        </div>
        <div class="confeti confeti2">
            <img :src="require('@/assets/confetti_right.png')"/>
        </div>

        <Modal :showModal="showModal" @close="handleCancelModal" :fullHeight="true">
            <CardDetails :type="modalCardType" />
        </Modal>
    </div>
</template>

<script>
import Loader from '@/components/loader';
import Modal from "@/components/play-modal";
import CardDetails from "@/components/card-details";
import VueCircle from 'vue2-circle-progress';
import { SCORES } from "@/utils/cards.ts";

export default {
    data(){
        return {
            loading: true,
            result: [],
            showModal: false,
            modalCardType: 1,
            resultCards: [],
            characteristic: SCORES,
        }
    },
    components: {
        Loader,
        Modal,
        CardDetails,
        VueCircle,
    },
    mounted(){
        const { resultId } = this.$route.params;
        if(!resultId){
            this.returnHome();
        }
        this.getResult(resultId);
    },
    methods: {
        getResult(resultId){
            this.loading = true;
            this.$http.get("result/" + resultId, this.account).then(
                response => {
                    const { user, result, picked } = response.body;
                    this.result = result;
                    this.loading = false;

                    var dump_picked = JSON.parse(JSON.stringify(picked));
                    var dump_characteristic = JSON.parse(
                        JSON.stringify(this.characteristic)
                    );
                    
                    console.log(dump_picked, dump_characteristic);
                    this.generateResult(dump_picked, dump_characteristic);
                },
                error => {
                    console.log('error', error);
                    this.returnHome();
                    this.loading = false;
                }
            );
        },
        returnHome(){
            this.$router.push("/dashboard");
        },
        handleOpenCard(card) {
            this.modalCardType = card.type;
            this.showModal = true;
            document.getElementsByTagName('body')[0].style.overflowY = "hidden";
        },
        handleCancelModal() {
            this.showModal = false;
            document.getElementsByTagName('body')[0].style.overflowY = "auto";
        },

        //
        generateResult(cardss, result) {
            let all_answers = [];
            let results = [...result];
            let cards = [...cardss];
            let empty_totals_added = [];

            //convert response type to array
            cards.map(card => {
                card.answers.map(ans => {
                if (isNaN(Number(ans.type))) {
                    const types = ans.type.split(",");
                    const types_number = types.map(t => {
                    return Number(t);
                    });
                    ans.type = types_number;
                } else {
                    ans.type = Number(ans.type);
                }
                });
            });

            // get all numbers
            cards.map(card => {
                card.answers.map(item => {
                if (typeof item.type === "number") {
                    all_answers = [...all_answers, item.type];
                } else {
                    all_answers = [...all_answers, ...item.type];
                }
                });
            });

            // total all numbers
            all_answers.map(type => {
                if (type !== 0) {
                const item = results.find(data => data.type == type);
                const index = results.indexOf(item);

                if (results[index].total) {
                    results[index].total++;
                } else {
                    results[index].total = 1;
                }
                }
            });

            // add 0 to all no totals
            results.map(result => {
                if (!result.total) {
                result.total = 0;
                }
            });

            // sort
            const sorted_results = results.sort((a, b) => {
                if (a.total > b.total) return -1;
                return a.total < b.total ? 1 : 0;
            });

            this.getPercentage(sorted_results);
        },
        getPercentage(cards) {
            let baseTotal = 0;
            cards.map(card => (baseTotal += card.total));

            cards.map(card => {
                card.percentage = Math.round((card.total / baseTotal) * 100);
                card.percentageOrig = (card.total / baseTotal) * 100;
            });

            let percentageTotal = 0;
            cards.map(card => (percentageTotal += card.percentage));

            this.resultCards = [...cards];
            console.log(this.resultCards);
        },
        getIndividualPercent(card){
            if(!this.resultCards.length){
                return false;
            } else {
                const item = this.resultCards.find(result => result.type === card.type);
                return item.percentage;
            }
        }
    }
}
</script>

<style lang="scss" scoped>
.card-parent-result {
    position: relative;
    background-image: url('~@/assets/assessment_bg.png');
    background-position: top center;
    background-repeat: no-repeat;
    max-width: 1401px;
    width: 100%;
    height: 100vh;
    margin: auto;

  h2.title {
    padding-top: 24px;
    padding-bottom: 15px;
    text-align: center;
    font: normal normal 600 26px/24px Crimson Pro;
    letter-spacing: 0px;
    color: #221e1f;
    width: 100%;
    max-width: 1199px;
    margin: 0 auto;
    border-bottom: 1px solid var(--gray);
  }

    .card-list {
        margin-top: 33px;
        display: flex;
        align-items: center;
        justify-content: center;

        .top {
            margin: 0 0 10px 0;
            font-size: 20px;
            text-align: center;
            font-weight: bold;
            color: #2F281E;
        }

        .content {
            position: relative;
            cursor: pointer;
            img {
                width: 100%;
                height: 100%;
            }
            .percent {
                position: absolute;
                bottom: 20%;
                left: 0;
                right: 0;
                margin: auto;
                width: 60px;
                height: 60px;
                overflow: hidden;
                background-color: #fff;
                border-radius: 100%;
                .percent-label {
                    display: flex;
                }
                .percent-text {
                    color: #00AEEF;
                    font-size: 18px;
                }
                .percent-perc {
                    color: #00AEEF;
                    font-size: 9px;
                    line-height: 3;
                }
            }
        }

        .card {
            &-big {
                margin: 0 5px 0;
                .content {
                    width: 210px;
                    height: 280px;
                }
            }
            &-small {
                margin: 0 5px 0;
                .content {
                    width: 150px;
                    height: 180px;
                }
            }
        }
    }

    .button {
        margin-top: 19px;
        text-align: center;
        button {
            color: #00AEEF;
            border-color: #00AEEF;
        }
    }

    .confeti {
        position: absolute;
        width: 0;
        height: 0;
        width: 252px;
        height: 260px;

        img {
            width: 100%;
            height: 100%;
        }

        &.confeti1 {
            bottom: 0;
            left: 0;
        }

        &.confeti2 {
            bottom: 0;
            right: 0;
        }
    }
}
</style>