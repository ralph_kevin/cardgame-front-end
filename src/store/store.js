import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

const store = new Vuex.Store({
  plugins: [createPersistedState()],
  state: {
    user: {
      id: "",
      username: "",
      email: "",
      verified: false,
      token: "",
      description: ""
    },
    selectedCards: {
      selectedAssesmentCards: [],
      selectedSummaryCards: []
    },
    language: "en",
    ticketId: null,
    planId: null,
    alertStatus: false
  },
  mutations: {
    loginUser: (state, payload) => {
      state.user = payload;
    },
    logoutUser: state => {
      state.user = {
        id: "",
        unsername: "",
        email: "",
        verified: false,
        token: ""
      };
    },
    verifyUser: state => {
      state.user.verified = true;
    },
    setSelectedAssesmentCards: (state, payload) => {
      state.selectedCards.selectedAssesmentCards = payload;
    },
    resetSelectedAssesmentCards: state => {
      state.selectedCards.selectedAssesmentCards = [];
      state.selectedCards.selectedSummaryCards = [];
    },
    setSelectedSummaryCards: (state, payload) => {
      state.selectedCards.selectedSummaryCards = payload;
    },
    resetSelectedSummaryCards: state => {
      state.selectedCards.selectedSummaryCards = [];
    },
    setActiveLanguage: (state, value) => {
      state.language = value;
    },
    setTicketId: (state, value) => {
      state.ticketId = value;
    },
    setPlanId: (state, value) => {
      state.planId = value;
    },
    setAlertStatus: (state, value) => {
      state.alertStatus = value;
    },
  },
  getters: {
    user: state => {
      return state.user;
    },
    getSelectedCards: state => {
      return state.selectedCards.selectedAssesmentCards;
    },
    getSelectedSummaryCards: state => {
      return state.selectedCards.selectedSummaryCards;
    },
    getActiveLanguage: state => {
      return state.language;
    },
    getTicketId: state => {
      return state.ticketId;
    },
    getPlanId: state => {
      return state.planId;
    },
    alertStatus: state => {
      return state.alertStatus;
    }
  }
});

export default store;
