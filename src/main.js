import Vue from "vue";
import App from "./App.vue";
import VueRouter from "vue-router";
import VueResource from "vue-resource";
import { routes } from "@/router/index";
import store from "./store/store";
import "./styles/reset.css";
import "./styles/index.scss";
import "./styles/mixin.scss";
import { BootstrapVue } from "bootstrap-vue";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import { API_SERVERS } from "@/utils/constant.ts";
import VueMq from "vue-mq";
import vuexI18n from 'vuex-i18n';
import { LANGUAGES } from "./utils/languages.ts";

const router = new VueRouter({
  routes: routes,
  mode: "history",
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    }

    if (to.hash) {
      return { selector: to.hash };
    }
    return { x: 0, y: 0 };
  }
  // fixes router on scroll in pages
});

Vue.use(VueRouter);
Vue.use(VueResource);
Vue.http.options.root = API_SERVERS.live;

Vue.filter("two_digits", value => {
  if (value < 0) {
    return "00";
  }
  if (value.toString().length <= 1) {
    return `0${value}`;
  }
  return value;
});

Vue.use(BootstrapVue);

var VueScrollTo = require("vue-scrollto");
Vue.use(VueScrollTo);

Vue.use(VueMq, {
  breakpoints: {
    mobile: 992,
    desktop: Infinity
  }
});

Vue.use(vuexI18n.plugin, store);
Vue.i18n.add('en', LANGUAGES['en']);
Vue.i18n.add('cn', LANGUAGES['cn']);
Vue.i18n.set(store.getters.getActiveLanguage);
// this.$i18n.set(data.id);

new Vue({
  el: "#app",
  router: router,
  store: store,
  render: h => h(App)
});
