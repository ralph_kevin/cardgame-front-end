const API_SERVERS = {
    live: 'http://13.213.4.206/server/api',
    local: 'http://127.0.0.1:8000/api',
};

export {
    API_SERVERS,
}