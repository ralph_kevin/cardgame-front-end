const LANGUAGES = {
  en: {
    // no need to add here
  },
  cn: {
    Home: "首頁",
    Result: "結果",
    "Take a Test": "測驗一次",
    // Home
    "Discovering the Gem that You Are": "發現您自己的寶藏。",
    "Know and understand the type of student you are based on interests. Guiding you onto the first step to your future – your career path.":
      "挖掘日常活動的興趣與風格，看見自己的特色與天賦",
    "About Us": "關於我們",
    "How it Works": "如何進行測驗？",
    "Contact Us": "聯繫我們",
    "Get Started": "開始測驗",
    "Watch the Video": "觀看影片",
    // Sign
    "Sign In": "登錄",
    "Guiding you onto the first step to your future – your career path.":
      "引導您踏出通往未來方向的第一步: 您的生涯道路。",
    "Sign in with Classmade": "透過Classmade登錄",
    "Email Address": "電子郵件",
    Password: "密碼",
    "Forgot Password?": "忘記密碼",
    "Don't have an account?": "尚未擁有帳號？",
    Join: "註冊",
    "Welcome Back": "歡迎回到尋嘗日",
    "Sign up on NAME": "透過姓名註冊",
    "Just a few clicks away to the best career path for you.":
      "在多點擊幾下滑鼠，就能找到最適合您的生涯道路。",
    Name: "姓名",
    "Re-type Password": "重複輸入密碼",
    "password must be atleast 8 characters": "密碼必須是至少8位元",
    "I have accepted the": "我已同意接受",
    "Terms and Conditions": "條款和條件",
    "Sign Up": "註冊",
    "Already have an account?": "已經擁有帳號？ 登錄",
    Verification: "開始驗證",
    "Thank you for signing up!": "謝謝您的註冊！",
    "Enter here the verification code we’ve sent to your email.":
      "在此輸入驗證碼，請到您的電子郵箱查收。",
    "Resend Code": "重新寄送驗證碼",
    Verify: "確認",

    // Packages
    "You’re a step closer! Select the plan that works best for you!":
      "你又完成了一步！選擇最適合您的計畫！",
    "To continue with Find Life Interest, select the plan that works best for you. All license purchase gives you 4 chances to take the digital card game and valid only on the chosen plan.":
      "要繼續尋嘗日，請選擇最適合您的計劃。購買的所有許可證，都提供 4 次參加測驗的權限，並且僅對所選計劃有效。",
    "1 Year Plan": "一年方案",
    "Eligible for four (4) takes only and license valid for one year":
      "僅擁有測驗次數4次和有效期間為一年",
    "2 Year Plan": "兩年方案",
    "Eligible for four (4) takes only and license valid for two years":
      "僅擁有測驗次數8次和有效期間為二年",
    "3 Year Plan": "三年方案",
    "Eligible for four (4) takes only and license valid for three years":
      "僅擁有測驗次數12次和有效期間為三年",
    "Choose Plan": "選擇方案",
    "Questions?": "問題？",
    "Set up your credit card or debit card": "選擇使用信用卡或金融卡",
    "First Name": "名字",
    "Last Name": "姓氏",
    "Card Number": "卡片號碼",
    "Expiration Date (MM/YY)": "有效期限",
    "Security Code (CVV)": "安全碼",
    "Change Plan Details": "更改方案",
    "Your payments will be processed internationally. Additional bank fees may apply.":
      "您的付款將為跨國交易，銀行可能會收取額外的匯率差與手續費。",
    "Start Membership": "",
    Language: "語言",
    Currency: "幣別",
    "Log Out": "登出",
    // dashboard
    Subscription: "訂閱",
    "Plan Expiration": "有效期限:",
    "About me": "關於我們",
    "Update Profile": "更新資料",
    // "Hello there! ": "",
    "Write something you wanna share about yourself...":
      "寫一些您想分享的關於您自己的東西......",

    "No Result Found": "沒有找到資料",
    "Wait! You’re assessment is not yet done! ": "等一下！您的分析測驗尚未完成",
    "If you wish to close it now, you’ll lose all the data and will need to start over again.":
      "您如果現在要關閉視窗，您將會丟失所有的資料和需要重頭開始！",
    Return: "返回",
    Close: "關閉",
    "Your character type is:": "您的人格特質：",
    "Latest Result": "最新的結果",
    Static: "靜態",
    "Non-Static": "動態",
    "Selected Card": "已選擇",
    // Contact
    "Get In Touch": "保持聯繫",
    "Reason / Problem": "理由,/問題",
    "What do you think about this game?": "您覺得這款遊戲怎麼樣？",
    "Your ": "你的",
    // How it works
    "How It Works": "如何使用卡牌進行測驗？",
    "Step by Step": "步驟說明",
    "Choose cards": "選擇卡牌",
    "Out of the 54 display cards, choose 8 that you feel most connected to":
      "從54展示的卡牌當中，選擇8張最能代表您日常生活的卡。",
    "Select reasons": "選擇原因",
    "Pick all the reasons that relates to your chosen card topic. No limit in choosing!":
      "從您選擇的卡，挑選所有理由讓您選這幾張卡牌的原因。",
    Evaluation: "評估",
    "From the selected reasons, the system will evaluate all the reasons with corresponding points":
      "根據選擇的原因，演算法會對所有的原因進行相對應的評分",
    "The selected card with the highest point will be reflected as the character type result":
      "選擇中累積最高分的卡，將能反映出您的類型結果",
    // account
    "Account Details": "帳戶細節",
    "Plan Details": "方案細節",
    "Update Account details": "更新帳戶細節",
    "Change Password": "變更密碼",
    "Update Plan Details": "更新方案細節",
    Account: "帳號",

    Address: "地址",
    "School / Organization Name": "學校/組織名稱",

    Plan: "	方案",
    "Expiration Date": "到期日",
    Payment: "付款",
    Cancel: "取消",
    "Save Changes": "儲存變更",
    "Current Password": "目前密碼",
    "New Password": "新的密碼",
    "Confirm Password": "確認密碼",

    "Profile image": "帳戶照片",
    "Upload image": "上傳照片",
    // card assessment
    "Card Assessment": "卡牌測驗",
    "Pick a card that represents you or your hobbies. You can only select maximum of 10 cards":
      "挑選一張卡牌是能夠代表您或您的興趣，最多只能選擇10張卡牌。",
    "Card Summary": "卡牌簡介",
    Remove: "移除",
    Next: "下一張",
    "Select and Flip a card to see questionnaire": "選擇和翻這張牌看問題",
    Complete: "完成",
    Back: "返回",
    Done: "完成",
    "Select your Top 3 Cards": "選擇您最高的3張卡牌",
    "Result Summary": "結果總結",
    "Assessment Result": "測驗結果",
    "Return Home": "返回首頁",
    Select: "選擇",
    "Tick off from the statements below the reasons why you picked out this card.":
      "勾選您選擇這張卡牌的理由",
    // welcome
    "Welcome to": "歡迎回到",
    "Interest and preference": "興趣偏好",
    "Technical and Vocational Education Academic Subjects to Consider “Technical and Vocational Subjects to Consider”":
      "參考的技職15群科",
    "University Academic Subjects to Consider": "參考的大學18學群",
    "Sample Careers": "參考職業領域",
    "A digital card game that will help you discover the gem that you are. With this creatively crafted game, you will be able to gain a deeper understanding of who you are and what strategies will work best for your growth and development":
      "一款線上卡牌測驗，將幫助您發現自己的寶藏。通過這款創造性地製作的遊戲，您將能夠更深入地了解自己是誰，以及哪種策略最適合您個人的成長和發展",
"Description of qualities:": '質性的說明：',
"Interests and preferences:": '興趣偏好：',
"Sample careers:": '參考職業領域：',
"Technical and vocational education academic subjects to consider (out of 15 subject groups):": '技職15群科：',
"University academic subjects to consider (out of 18 subject groups):": '大學18學群：',
"Personality traits:": '特質形容詞參考：',
"Holland Code:": '何倫碼(Holland)：',
  }
};

export { LANGUAGES };
