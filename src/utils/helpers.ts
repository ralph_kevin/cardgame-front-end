import store from "../store/store";

export const Helpers = {
  isUserLogged: function() {
    return store.state.user.verified;
  },
  anchorHashCheck: function(thash) {
    if (window.location.hash === thash) {
      const el = document.getElementById(thash.slice(1));
      if (el) {
        window.scrollTo(0, el.offsetTop);
      }
    }
  }
};
