# cardgame

> CardGame Project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run serve

# build for production with minification
npm run build

# intall vuese documentation
yarn global add @vuese/cli

# generate documentation
vuese gen

# serve documentation
vuese serve --open
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).
