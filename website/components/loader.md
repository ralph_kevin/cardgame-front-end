# loader

## Props

<!-- @vuese:loader:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|loading|-|`Boolean`|`false`|false|
|size|-|`Number`|`false`|128|
|stroke|-|`String`|`false`|#007bff|

<!-- @vuese:loader:props:end -->


