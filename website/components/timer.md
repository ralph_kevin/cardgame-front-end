# timer

## Props

<!-- @vuese:timer:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|date|-|`String`|`false`|-|

<!-- @vuese:timer:props:end -->


## Events

<!-- @vuese:timer:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|onEnd|-|-|

<!-- @vuese:timer:events:end -->


