# one-by-one

## Props

<!-- @vuese:one-by-one:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|cards|-|`Array`|`false`|[]|
|selected|-|`Array`|`false`|[]|

<!-- @vuese:one-by-one:props:end -->


## Events

<!-- @vuese:one-by-one:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|removeItem|-|-|

<!-- @vuese:one-by-one:events:end -->


