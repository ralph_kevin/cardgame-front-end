# spread-out

## Props

<!-- @vuese:spread-out:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|cards|-|`Array`|`false`|[]|
|selected|-|`Array`|`false`|[]|

<!-- @vuese:spread-out:props:end -->


## Events

<!-- @vuese:spread-out:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|removeItem|-|-|

<!-- @vuese:spread-out:events:end -->


