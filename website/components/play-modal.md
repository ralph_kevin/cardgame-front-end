# play-modal

## Props

<!-- @vuese:play-modal:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|showModal|-|`Boolean`|`false`|false|
|fullHeight|-|`Boolean`|`false`|false|
|founder|-|`Boolean`|`false`|false|

<!-- @vuese:play-modal:props:end -->


## Events

<!-- @vuese:play-modal:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|close|-|-|

<!-- @vuese:play-modal:events:end -->


## Slots

<!-- @vuese:play-modal:slots:start -->
|Name|Description|Default Slot Content|
|---|---|---|
|default|-|-|

<!-- @vuese:play-modal:slots:end -->


