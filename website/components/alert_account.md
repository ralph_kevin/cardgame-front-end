# alert_account

## Props

<!-- @vuese:alert_account:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|show|-|`Boolean`|`false`|false|
|value|-|`String`|`false`|-|
|propStyle|-|`Object`|`false`|{}|

<!-- @vuese:alert_account:props:end -->


## Events

<!-- @vuese:alert_account:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|close|-|-|

<!-- @vuese:alert_account:events:end -->


