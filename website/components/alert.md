# alert

## Props

<!-- @vuese:alert:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|show|-|`Boolean`|`false`|false|
|value|-|`String`|`false`|-|
|propStyle|-|`Object`|`false`|{}|

<!-- @vuese:alert:props:end -->


## Events

<!-- @vuese:alert:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|close|-|-|

<!-- @vuese:alert:events:end -->


