# close-modal

## Props

<!-- @vuese:close-modal:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|active|-|`Boolean`|`false`|true|
|head|-|`String`|`false`|Wait! You’re assessment is not yet done!|
|desc|-|`String`|`false`|If you wish to close it now, you’ll lose all the data and will need to start over again.|

<!-- @vuese:close-modal:props:end -->


## Events

<!-- @vuese:close-modal:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|return|-|-|
|disregard|-|-|

<!-- @vuese:close-modal:events:end -->


