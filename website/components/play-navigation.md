# play-navigation

## Props

<!-- @vuese:play-navigation:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|disabledNext|-|`Boolean`|`false`|true|
|isFixed|-|`Boolean`|`false`|true|
|nextText|-|`String`|`false`|Next|
|cancelText|-|`String`|`false`|Cancel|

<!-- @vuese:play-navigation:props:end -->


## Events

<!-- @vuese:play-navigation:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|cancel|-|-|
|next|-|-|

<!-- @vuese:play-navigation:events:end -->


## Slots

<!-- @vuese:play-navigation:slots:start -->
|Name|Description|Default Slot Content|
|---|---|---|
|default|-|-|

<!-- @vuese:play-navigation:slots:end -->


