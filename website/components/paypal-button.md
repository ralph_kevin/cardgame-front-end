# paypal-button

## Props

<!-- @vuese:paypal-button:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|plans|-|`String` /  `Object`|`false`|{
                id: 0,
                plan_id: '',
                total_months: 0,
                hkd: 1,
                length_description: '',
                plan_description: '',
            }|

<!-- @vuese:paypal-button:props:end -->


